#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>


// OUTPUTS oF THE DIFFERENTIAL JOYSTICK FUNCTION
float     nMotMixL = 0;           // Motor (left)  mixed output           (-128..+127)
float     nMotMixR = 0;           // Motor (right) mixed output           (-128..+127)


 //ID OF THE ESP
int myID = 1;

//LISTENING ON PORT
unsigned int localPort = 9000;

//SENDING ON PORT
unsigned int sendPort = 53000; 

//Manage Timer for sending messages out
long lastSendTime = 0;
long sendDelay = 500;

OSCErrorCode error;

//This is the actual router the devices coonect to (Mirko's Home Network).
const char* ssid     = "EE-BrightBox-dm5had";
const char* password = "lab-flock-late";

//buffer to hold incoming packet
//char packetBuffer[255]; 
//IPAddress broadcast(255,255,255,255);
IPAddress MyIp;

//IPAddress box2IP(192,168,1,111);
//IPAddress box3IP(192,168,1,96);

IPAddress remoteIp;

//SETUP UDP
WiFiUDP Udp;

void setup() {

  Serial.begin(115200);
  delay(2000);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  MyIp = WiFi.localIP();
  Serial.println(MyIp);

  //START THE UDP CONNECTION
  if(Udp.begin(localPort))
  {
    Serial.println("Listening on port: ");
    Serial.println(localPort);
  }

  //pinMode(credenzaLRelayPin, OUTPUT);
  //pinMode(growingStripLRedPin, OUTPUT);
  //pinMode(lightBoxPin, OUTPUT);
  pinMode(0, OUTPUT);

  //TURN THE HUZZAH RED LED ON 
  digitalWrite(0, LOW);
  
}

//DECLARE ALL FUNCTIONS TRIGGERED BY OSC MESSAGES

//MOVEMENT FUNCTIONS
void mover(OSCMessage &msg) {
  
  float f1 = msg.getFloat(0);
  float f2 = msg.getFloat(1);
  float f3 = msg.getFloat(2);
  //float f4 = msg.getFloat(3);

  steering(f1,f2);

  String s = "s";
  s+=",";
  s+="wheels";
  s+=",";
  s+=String(nMotMixL,4);
  s+=",";
  s+=String(nMotMixR,4);
  s+=",";
  s+=String(f3,4);
  //s+=",";
  //s+=String(f4,4);
  s+="\n";
  Serial.print(s);
  //delay(15);
}

//STEPPER CONTROL
void stepper(OSCMessage &msg) {
  
  float f1 = msg.getFloat(0);
  float f2 = msg.getFloat(1);
  float f3 = msg.getFloat(2);

  String s = "s";
  s+=",";
  s+="step";
  s+=",";
  s+=String(f1,4);
  s+=",";
  s+=String(f2,4);
  s+=",";
  s+=String(f3,4);
  s+="\n";
  Serial.print(s);
  //delay(15);
}

void stepperTo(OSCMessage &msg) {
  
  float f1 = msg.getFloat(0);
  float f2 = msg.getFloat(1);
  float f3 = msg.getFloat(2);

  String s = "s";
  s+=",";
  s+="stepTo";
  s+=",";
  s+=String(f1,4);
  s+=",";
  s+=String(f2,4);
  s+=",";
  s+=String(f3,4);
  s+="\n";
  Serial.print(s);
  //delay(15);
}

void stepperStop(OSCMessage &msg) {
  
  float f1 = msg.getFloat(0);

  String s = "s";
  s+=",";
  s+="stepStop";
  s+=",";
  s+=String(f1,4);
  s+="\n";
  Serial.print(s);
  //delay(15);
}

void stepperENABLE_DISABLE(OSCMessage &msg) {
  
  float f1 = msg.getFloat(0);

 /* Serial.print(f1, 4);
  Serial.print(" ");
  Serial.println(f2, 4);*/

  String s = "s";
  s+=",";
  s+="stepEnable";
  s+=",";
  s+=String(f1,4);
  s+="\n";
  Serial.print(s);
  //delay(15);
}

//PLAY MUSIC
void playMusic(OSCMessage &msg) {
   
  float f1 = msg.getFloat(0);
  float f2 = msg.getFloat(1);
  
  String s = "s";
  s+=",";
  s+="play";
  s+=",";
  s+=String(f1,4);
  s+=",";
  s+=String(f2,4);
  s+="\n";
  Serial.print(s);
  //delay(15);
}

//STOP MUSIC
void stopMusic(OSCMessage &msg) {
  
  float f1 = msg.getFloat(0);
  float f2 = msg.getFloat(1);
  
  String s = "s";
  s+=",";
  s+="stop";
  s+=",";
  s+=String(f1,4);
  s+=",";
  s+=String(f2,4);
  s+="\n";
  Serial.print(s);
  //delay(15);
}

//PAUSE MUSIC
void pauseMusic(OSCMessage &msg) {
  
  float f1 = msg.getFloat(0);
  float f2 = msg.getFloat(1);
  
  String s = "s";
  s+=",";
  s+="pause";
  s+=",";
  s+=String(f1,4);
  s+=",";
  s+=String(f2,4);
  s+="\n";
  Serial.print(s);
  //delay(15);
}


void ping(OSCMessage &msg) {

    //SEND ALL INFO TO HOST
    Serial.print("Ping");
}


void loop() {

  //CONTINUOSLY CHECK FOR INCOMING PACKETS
  int packetSize = Udp.parsePacket();

  //IF Packet is received
  if (packetSize > 0) {

    //CREATE MESSAGE TO FILL WITH INCOMING DATA
    OSCMessage msg;

    //Write Packet Size
    //Serial.print("Received packet of size: ");
    //Serial.println(packetSize);

    //Write where the packet cme from
    //Serial.print("From ");
    remoteIp = Udp.remoteIP();
    //Serial.print(remoteIp);
    //Serial.print(", port ");
    //Serial.println(Udp.remotePort());

    //Fill the empty OSC message with the incoming message
    while (packetSize--){
      msg.fill(Udp.read());
    }

    //Route the message
    if (!msg.hasError()) {
      msg.dispatch("/move", mover);
      msg.dispatch("/stepper", stepper);
      msg.dispatch("/stepperTo", stepperTo);
      msg.dispatch("/stepperStop", stepperStop);
      msg.dispatch("/stepperControl", stepperENABLE_DISABLE);
      
      msg.dispatch("/playMusic", playMusic);
      msg.dispatch("/stopMusic", stopMusic);
      msg.dispatch("/pauseMusic", pauseMusic);
      msg.dispatch("/ping", ping);
      
    } else {
      error = msg.getError();
      Serial.print("error: ");
      Serial.println(error);
    }

  }
  
}

void steering(float valx, float valy){
  
  
// Differential Steering Joystick Algorithm
// ========================================
//   by Calvin Hass
//   http://www.impulseadventure.com/elec/
//
// Converts a single dual-axis joystick into a differential
// drive motor control, with support for both drive, turn
// and pivot operations.
//

// INPUTS

float nJoyX = valx*127;
float nJoyY = valy*127;


// CONFIG
// - fPivYLimt  : The threshold at which the pivot action starts
//                This threshold is measured in units on the Y-axis
//                away from the X-axis (Y=0). A greater value will assign
//                more of the joystick's range to pivot actions.
//                Allowable range: (0..+127)
float fPivYLimit = 32.0;
      
// TEMP VARIABLES
float   nMotPremixL;    // Motor (left)  premixed output        (-128..+127)
float   nMotPremixR;    // Motor (right) premixed output        (-128..+127)
int     nPivSpeed;      // Pivot Speed                          (-128..+127)
float   fPivScale;      // Balance scale b/w drive and pivot    (   0..1   )


// Calculate Drive Turn output due to Joystick X input
if (nJoyY >= 0) {
  // Forward
  nMotPremixL = (nJoyX>=0)? 127.0 : (127.0 + nJoyX);
  nMotPremixR = (nJoyX>=0)? (127.0 - nJoyX) : 127.0;
} else {
  // Reverse
  nMotPremixL = (nJoyX>=0)? (127.0 - nJoyX) : 127.0;
  nMotPremixR = (nJoyX>=0)? 127.0 : (127.0 + nJoyX);
}

// Scale Drive output due to Joystick Y input (throttle)
nMotPremixL = nMotPremixL * nJoyY/128.0;
nMotPremixR = nMotPremixR * nJoyY/128.0;

// Now calculate pivot amount
// - Strength of pivot (nPivSpeed) based on Joystick X input
// - Blending of pivot vs drive (fPivScale) based on Joystick Y input
nPivSpeed = nJoyX;
fPivScale = (abs(nJoyY)>fPivYLimit)? 0.0 : (1.0 - abs(nJoyY)/fPivYLimit);

// Calculate final mix of Drive and Pivot
nMotMixL = (((1.0-fPivScale)*nMotPremixL + fPivScale*( nPivSpeed))/127);
nMotMixR = (((1.0-fPivScale)*nMotPremixR + fPivScale*(-nPivSpeed))/127);


}
