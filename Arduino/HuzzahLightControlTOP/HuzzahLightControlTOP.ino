#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>


 //ID OF THE ESP
int myID = 1;

int speedLeftValue = 0;
int speedRightValue = 0;

//LISTENING ON PORT
unsigned int localPort = 9000;

//SENDING ON PORT
unsigned int sendPort = 53000; 

//Manage Timer for sending messages out
long lastSendTime = 0;
long sendDelay = 500;

OSCErrorCode error;

//This is the actual router the devices coonect to (Mirko's Home Network).
const char* ssid     = "EE-BrightBox-dm5had";
const char* password = "lab-flock-late";

//buffer to hold incoming packet
char packetBuffer[255]; 
IPAddress broadcast(255,255,255,255);
IPAddress MyIp;

//IPAddress box2IP(192,168,1,111);
//IPAddress box3IP(192,168,1,96);

IPAddress remoteIp;

//SETUP UDP
WiFiUDP Udp;

void setup() {

  Serial.begin(9600);
  delay(2000);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  MyIp = WiFi.localIP();
  Serial.println(MyIp);

  //START THE UDP CONNECTION
  if(Udp.begin(localPort))
  {
    Serial.println("Listening on port: ");
    Serial.println(localPort);
  }

  //pinMode(credenzaLRelayPin, OUTPUT);
  //pinMode(growingStripLRedPin, OUTPUT);
  //pinMode(lightBoxPin, OUTPUT);
  pinMode(0, OUTPUT);

  //TURN THE HUZZAH RED LED ON 
  digitalWrite(0, LOW);
  
}

//DECLARE ALL FUNCTIONS TRIGGERED BY OSC MESSAGES
//GO FORWARD
void dim(OSCMessage &msg) {
  speedLeftValue = msg.getInt(0);
  speedRightValue = msg.getInt(1);
  Serial.print('D');
  Serial.write(speedLeftValue);
  Serial.write(speedRightValue);
}

//GO BCKWARD
void tilt(OSCMessage &msg) {
  speedLeftValue = msg.getInt(0);
  speedRightValue = msg.getInt(1);
  Serial.print('T');
  Serial.write(speedLeftValue);
  Serial.write(speedRightValue);
}


void ping(OSCMessage &msg) {

    //SEND ALL INFO TO HOST
    Serial.print("Ping");
}


void loop() {

  //CONTINUOSLY CHECK FOR INCOMING PACKETS
  int packetSize = Udp.parsePacket();

  //IF Packet is received
  if (packetSize > 0) {

    //CREATE MESSAGE TO FILL WITH INCOMING DATA
    OSCMessage msg;

    //Write Packet Size
    //Serial.print("Received packet of size: ");
    //Serial.println(packetSize);

    //Write where the packet cme from
    //Serial.print("From ");
    remoteIp = Udp.remoteIP();
    //Serial.print(remoteIp);
    //Serial.print(", port ");
    //Serial.println(Udp.remotePort());

    //Fill the empty OSC message with the incoming message
    while (packetSize--){
      msg.fill(Udp.read());
    }

    //Route the message
    if (!msg.hasError()) {
      msg.dispatch("/light/dim", dim);
      msg.dispatch("/light/tilt", tilt);
      
      msg.dispatch("/ping", ping);
      
    } else {
      error = msg.getError();
      Serial.print("error: ");
      Serial.println(error);
    }

  }
  
}
