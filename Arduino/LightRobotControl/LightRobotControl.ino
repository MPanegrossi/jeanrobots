#include <SoftwareSerial.h>

#include <Arduino.h>
#include "BasicStepperDriver.h"

#include <DRV8835MotorShield.h>

#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>

int minDistance = 50;

//----------------MOTORS SETUP

DRV8835MotorShield motors;
//pins 10,9,8,7,6

// Motor steps per revolution. Most steppers are 200 steps or 1.8 degrees/step
#define MOTOR_STEPS 200

//SETTINGS FOR THE STEPPER MOTOR

// All the wires needed for full functionality
#define DIR 2
#define STEP 19
//Uncomment line to use enable/disable functionality
#define ENBL 5

// Since microstepping is set externally, make sure this matches the selected mode
// 1=full step, 2=half step etc.
#define MICROSTEPS 1

//Uncomment line to use enable/disable functionality
BasicStepperDriver stepper(MOTOR_STEPS, DIR, STEP, ENBL);


//-------------- SOFTWARESERIAL SETUP
/*
// Define SoftSerial TX/RX pins
// Connect Arduino pin 8 to TX of usb-serial device
uint8_t ssRX = 2;
// Connect Arduino pin 9 to RX of usb-serial device
uint8_t ssTX = 19;*/

//SoftwareSerial Serial(ssRX, ssTX);

//----------------AUDIO BOARD SETUP

// These are the pins used for the breakout example
#define BREAKOUT_RESET  15      // VS1053 reset pin (output)
#define BREAKOUT_CS     16     // VS1053 chip select pin (output)
#define BREAKOUT_DCS    17      // VS1053 Data/command select pin (output)


// These are common pins between breakout and shield
#define CARDCS 4     // Card chip select pin
// DREQ should be an Int pin, see http://arduino.cc/en/Reference/attachInterrupt
#define DREQ 3       // VS1053 Data request, ideally an Interrupt pin

Adafruit_VS1053_FilePlayer musicPlayer = 
  // create breakout-example object!
  Adafruit_VS1053_FilePlayer(BREAKOUT_RESET, BREAKOUT_CS, BREAKOUT_DCS, DREQ, CARDCS);
  // create shield-example object!
  //Adafruit_VS1053_FilePlayer(SHIELD_RESET, SHIELD_CS, SHIELD_DCS, DREQ, CARDCS);

  String tracks[] = {"track002.mp3"};

//-----------END

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  //Serial.begin(9600);
 

    // uncomment one or both of the following lines if your motors' directions need to be flipped
  //motors.flipM1(true);
  //motors.flipM2(true);

  /*
     * Set target motor RPM.
     * These motors can do up to about 200rpm.
     * Too high will result in a high pitched whine and the motor does not move.
     */
    stepper.setRPM(100);
    
    motors.setM1Speed(0);
    motors.setM2Speed(0);

    stepper.enable();
    delay(200);
    stepper.disable();

  
  if (! musicPlayer.begin()) { // initialise the music player
     Serial.println(F("Couldn't find VS1053, do you have the right pins defined?"));
     while (1);
  }
  Serial.println(F("VS1053 found"));
  
  SD.begin(CARDCS);    // initialise the SD card
  
  // Set volume for left, right channels. lower numbers == louder volume!
  musicPlayer.setVolume(20,20);

  // Timer interrupts are not suggested, better to use DREQ interrupt!
  //musicPlayer.useInterrupt(VS1053_FILEPLAYER_TIMER0_INT); // timer int

  // If DREQ is on an interrupt pin (on uno, #2 or #3) we can do background
  // audio playing
  musicPlayer.useInterrupt(VS1053_FILEPLAYER_PIN_INT);  // DREQ int
  
  // Play one file, don't return until complete
  //Serial.println(F("Playing track 001"));
  //musicPlayer.playFullFile("track001.mp3");

 pinMode(15,OUTPUT);
 pinMode(16,OUTPUT);
 pinMode(17,OUTPUT);
 pinMode(18,OUTPUT);
 pinMode(19,OUTPUT);

}

void loop() {
  
  int sensorValue = analogRead(A0);
  //Serial.println(sensorValue);
  if(sensorValue < minDistance)
  {
      motors.setM1Speed(0);
      motors.setM2Speed(0);
      stepper.disable();
      
  } else {
  
    byte b = 0;
    //Receive Bytes from Pocessing
    if(Serial.available() > 2)
    {
      b = Serial.read();
      //Serial.flush();
      //Serial.println(b);
    }
  
    //--------- EVALUATION Commands from Processing
    byte leftSpeed  = 0;
    byte rightSpeed = 0;
    switch (b)
    { 
      
    //Receive 'F'
    case 70:
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("F");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2);
    break;  
    
    //Receive 'B'
    case 66: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("B");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2*(-1));
      motors.setM2Speed(rightSpeed*2*(-1));
    break; 
    
    //Receive 'L'
    case 76: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("L");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2);
    break; 
    
    //Receive 'R'
    case 82: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("R");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2);
    break; 
    
    //Receive 'S'
    case 83: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("S");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //STOP MOTOTRS
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2);
    break; 
    
     //Receive 'Q'
     case 81: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("Q");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2*(-1));
    break;
    
     //Receive 'W'
     case 87: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("W");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2*(-1));
      motors.setM2Speed(rightSpeed*2);
     break; 

     //Receive 'C' stepper rotation clockwise
     case 67: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("C");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Set the stepper to move
      // energize coils - the motor will hold position
       //stepper.enable();
       stepper.setRPM((int)rightSpeed);
       stepper.move(((int) leftSpeed)*MICROSTEPS);
       //stepper.rotate(((int)leftSpeed)*10);
       delay(150); //wait a while to reduce inertia
       // pause and allow the motor to be moved by hand  
       //stepper.disable();
     break; 

      //Receive 'A' stepper rotation anti-clockwise
      case 65: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("A");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Set the stepper to move
       //stepper.enable();
       stepper.setRPM((int)rightSpeed);
       //stepper.rotate(((int)leftSpeed)*10*(-1));
       stepper.move(((int)leftSpeed)*(-1)*MICROSTEPS);
       // pause and allow the motor to be moved by hand
       delay(150); //wait a while to reduce inertia
       //stepper.disable();
     break; 

      //Receive 'H' stepper rotation halt
      case 72: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("H");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      // energize coils - the motor will hold position
      //stepper.enable();
      //stepper.disable();
     break; 

     //Receive 'e' stepper enable/disable
      case 101: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("e");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      if(((int)leftSpeed) == 1){
        // energize coils - the motor will hold position
        stepper.enable();
        //stepper.disable();
      } else if(((int)leftSpeed) == 0)
      {
        // release the coils - the motor will spin freely by hand
        //stepper.enable();
        stepper.disable();
      }
      minDistance = (int) rightSpeed;
     break; 
  
        //Receive 'y' play file
        case 121: 
        leftSpeed  = Serial.read();
        rightSpeed = Serial.read();
        Serial.println("y");
        Serial.println(leftSpeed);
        Serial.println(rightSpeed);
        //Play another file in the background, REQUIRES interrupts!
        //Serial.println(F("Playing track 002"));
        char track_name[50];
        tracks[leftSpeed].toCharArray(track_name, 50);
        musicPlayer.startPlayingFile(track_name);
       break; 
  
       //Receive 's' stop file
        case 115: 
        leftSpeed  = Serial.read();
        rightSpeed = Serial.read();
        Serial.println("s");
        Serial.println(leftSpeed);
        Serial.println(rightSpeed);
        //Play another file in the background, REQUIRES interrupts!
        musicPlayer.stopPlaying();
       break;
  
        //Receive 'p' pause file
        case 112: 
        leftSpeed  = Serial.read();
        rightSpeed = Serial.read();
        Serial.println("p");
        Serial.println(leftSpeed);
        Serial.println(rightSpeed);
        if (! musicPlayer.paused()) {
          Serial.println("Paused");
          musicPlayer.pausePlaying(true);
        } else { 
          Serial.println("Resumed");
          musicPlayer.pausePlaying(false);
        }
       break;
    }
  
  //--------- END EVALUATION Commands from Processing
  

  }    

  
}
