#include <SoftwareSerial.h>
#include <Servo.h> 


boolean isDimmed=false;
boolean isCenter=false;
boolean isCenterUP=false;
boolean isLeft=false;
boolean isRight=false;
boolean isUp=false;
boolean isDown=false;

byte amount = 0;
byte fade = 25;
byte leftSpeed;
byte rightSpeed;

int newAmount=0;

unsigned long lastUpdate=0; 
unsigned long updateInterval=0; 

int servoDestination = 90; //where to tilt the servo with every OSC command

class Sweeper
{
  Servo servo;               // the servo
  int pos;                   // current servo position 
  int increment;             // increment to move for each interval
  int  updateInterval;       // interval between updates
  unsigned long lastUpdate;  // last update of position
 
public: 
  Sweeper(int interval)
  {
    updateInterval = interval;
    increment = 1;
    pos=0;
  }
  
  void Attach(int pin)
  {
    servo.attach(pin);
  }
  
  void Detach()
  {
    servo.detach();
  }

  int getPos()
  {
    return this->pos;
  }

  void setInterval(int _interval)
  {
    this->updateInterval = _interval;
  }

//FUNCTIONS FOR BOTH SERVOS
  void init(int _pos)
  {
      pos = _pos; 
      servo.write(pos);

  }
  

//FUNCTIONS FOR BOTTOM SERVO -----------------------------------------------------------------------
  void Center(int centerPos)
  {
    int _centerPos=centerPos;
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(pos<_centerPos)
      {
        pos += increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }      
       if(pos>_centerPos)
      {
        pos -= increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       
       if(getPos()==_centerPos)
       {
        isCenter=false;
       }
    }
  }

  void turnLeft()
  {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()>0)
      {
        pos -= increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==0)
       {
        isLeft=false;
       }
    }
  }

  void turnRight()
  {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()<180)
      {
        pos += increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==180)
       {
        isRight=false;
       }
    }
  }

//END FUNCTIONS FOR BOTTOM SERVO -----------------------------------------------------------------------

  

//FUNCTIONS FOR UPPER SERVO -----------------------------------------------------------------------

  void CenterUP(int centerPos)
  {
    digitalWrite(13,HIGH);
    int _centerPos=centerPos;
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()<_centerPos)
      {
        pos += increment;
        servo.write(getPos());
        /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }      
       if(pos>_centerPos)
      {
        pos -= increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       
       if(getPos()==_centerPos)
       {
        isCenterUP=false;
        digitalWrite(13,LOW);
       }
    }
  }

  void UP()
  {
    digitalWrite(13,HIGH);
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()>70)
      {
        pos -= increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==70)
       {
        isUp=false;
        digitalWrite(13,LOW);
       }
    }
  }

  void DOWN()
  {
    digitalWrite(13,HIGH);
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()<110)
      {
        pos += increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==110)
       {
        isDown=false;
        digitalWrite(13,LOW);
       }
    }
  }
  
};

class Dimmer
{
  int amount;                // current servo position 
  int increment;             // increment to move for each interval
  int  updateInterval;       // interval between updates
  unsigned long lastUpdate;  // last update of position
 
public: 
  Dimmer(int interval)
  {
    updateInterval = interval;
    increment = 1;
    amount=0;
  }
  
  int getAmount()
  {
    return this->amount;
  }

  void setInterval(int _interval)
  {
    this->updateInterval = _interval;
  }

  //FUNCTIONS FOR BOTH SERVOS
  void init(int _amount)
  {
      amount = _amount;
  }
  

//FUNCTIONS FOR BOTTOM SERVO -----------------------------------------------------------------------
  void dim(int _amount, int _fade )
  {
    int a =_amount;
    setInterval(_fade);
    if((millis() - lastUpdate) > this->updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getAmount()<a)
      {
        amount += increment;
       }      
       if(getAmount()>a)
       {
        amount -= increment;
       }
    }
    analogWrite(11,getAmount());
    //Serial.println(getAmount());
  }
  
};

//----------------MOTORS SETUP

Sweeper servoTilt(50);
Dimmer dimmer(25);


//-------------- SOFTWARESERIAL SETUP

// Define SoftSerial TX/RX pins
// Connect Arduino pin 6 to TX of usb-serial device
uint8_t ssRX = 6;
// Connect Arduino pin 7 to RX of usb-serial device
uint8_t ssTX = 7;

SoftwareSerial xbee(ssRX, ssTX);

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  xbee.begin(9600);
  
  // Attach a servo to pin #10
  servoTilt.Attach(10);

  //Set the servo to its center position
  servoTilt.init(90);
  dimmer.init(0);

}

void loop() {

  //Receive Bytes from Pocessing
  byte b = 0;
  if(xbee.available() > 2)
  {
    b = xbee.read();
    //Serial.println(b);
  }

  //--------- EVALUATION Commands from Processing
  switch (b)
  {
    
    case 68: 
    //DIMMER
      amount  = xbee.read();
      fade = xbee.read();
      Serial.println("D");
      Serial.println(amount);
      Serial.println(fade);

    break;  

    case 84: 
    //TILTER
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();
      Serial.println("T");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      if(rightSpeed == 1)
      {
        isDown = true;
        //servoDestination = leftSpeed;
        servoTilt.setInterval(leftSpeed);
      }
      if(rightSpeed == 2)
      {
        isUp = true;
        //servoDestination = leftSpeed;
        servoTilt.setInterval(leftSpeed);
      }
      if(rightSpeed == 0)
      {
        isCenterUP = true;
        servoTilt.setInterval(leftSpeed);
      }

    break; 

  }

//--------- END EVALUATION Commands from Processing

//MOVE THE SERVO
 if(isCenter || isCenterUP)
  {
      isLeft=false;
      isRight=false;
      isUp=false;
      isDown=false;
      servoTilt.CenterUP(90);
      //Serial.println(sweeper2.getPos());
  }

  if(isUp)
  {
    isCenter=false;
    isCenterUP=false;
    isLeft=false;
    isRight=false;
    isDown=false;
    servoTilt.UP();
    
   // Serial.println(sweeper1.getPos());
  }
  
  if(isDown)
  {
    isCenter=false;
    isCenterUP=false;
    isLeft=false;
    isRight=false;
    isUp=false;
    servoTilt.DOWN();
   // Serial.println(sweeper1.getPos());
  }

  dimmer.dim((int)amount,((int)fade));
  
    
}


