#include <SoftwareSerial.h>

#include <Arduino.h>
#include <DRV8835MotorShield.h>

#include <AccelStepper.h>

//----------------MOTORS SETUP
AccelStepper stepper(AccelStepper::DRIVER, 3, 4);

DRV8835MotorShield motors;

// Motor steps per revolution. Most steppers are 200 steps or 1.8 degrees/step
//#define MOTOR_STEPS 200


//SETTINGS FOR THE STEPPER MOTOR

// All the wires needed for full functionality
//#define DIR 4
//#define STEP 3
//Uncomment line to use enable/disable functionality
//#define ENBL 2

// Since microstepping is set externally, make sure this matches the selected mode
// 1=full step, 2=half step etc.
//#define MICROSTEPS 1

//Uncomment line to use enable/disable functionality
//BasicStepperDriver stepper(MOTOR_STEPS, DIR, STEP, ENBL);

//-------------- SOFTWARESERIAL SETUP

// Define SoftSerial TX/RX pins
// Connect Arduino pin 8 to TX of usb-serial device
uint8_t ssRX = 11;
// Connect Arduino pin 9 to RX of usb-serial device
uint8_t ssTX = 12;

SoftwareSerial xbee(ssRX, ssTX);



void setup() {
  // put your setup code here, to run once:
  pinMode(2,OUTPUT); 
  Serial.begin(9600);
  xbee.begin(9600);

    // uncomment one or both of the following lines if your motors' directions need to be flipped
  //motors.flipM1(true);
  //motors.flipM2(true);

  /*
     * Set target motor RPM.
     * These motors can do up to about 200rpm.
     * Too high will result in a high pitched whine and the motor does not move.
     */
    //stepper.setRPM(100);
    
    motors.setM1Speed(0);
    motors.setM2Speed(0);

    /*digitalWrite(2,LOW); 
    delay(200);
    digitalWrite(2,HIGH); */

  /*  pinMode(14, OUTPUT);
    pinMode(15, OUTPUT);
    pinMode(16, OUTPUT);

    digitalWrite(14, LOW);
    digitalWrite(15, LOW);
    digitalWrite(16, LOW);*/

    //stepper.setMaxSpeed(0);
    stepper.setAcceleration(250);

 
}

void loop() {

  //stepper.setMicrostep(MICROSTEPS);
  
  byte b = 0;
  //Receive Bytes from Pocessing
  if(xbee.available() > 2)
  {
    b = xbee.read();
    //Serial.flush();
    //Serial.println(b);
  }

  //--------- EVALUATION Commands from Processing
  byte leftSpeed  = 0;
  byte rightSpeed = 0;
  switch (b)
  { 
    
    //Receive 'F'
    case 70:
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();
      Serial.println("F");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2);
    break;  
    
    //Receive 'B'
    case 66: 
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();
      Serial.println("B");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2*(-1));
      motors.setM2Speed(rightSpeed*2*(-1));
    break; 
    
    //Receive 'L'
    case 76: 
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();
      Serial.println("L");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2);
    break; 
    
    //Receive 'R'
    case 82: 
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();
      Serial.println("R");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2);
    break; 
    
    //Receive 'S'
    case 83: 
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();
      Serial.println("S");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //STOP MOTOTRS
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2);
    break; 
    
     //Receive 'Q'
     case 81: 
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();
      Serial.println("Q");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2);
      motors.setM2Speed(rightSpeed*2*(-1));
    break;
    
     //Receive 'W'
     case 87: 
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();
      Serial.println("W");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      //Connect the motors
      motors.setM1Speed(leftSpeed*2*(-1));
      motors.setM2Speed(rightSpeed*2);
     break; 

     //Receive 'C' stepper rotation clockwise
     case 67: 
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();
      //Set the stepper to move
      // energize coils - the motor will hold position
       digitalWrite(2,LOW); 
       stepper.setMaxSpeed(rightSpeed*100);
       //stepper.setRPM((int)rightSpeed);
       stepper.move(leftSpeed*100);
     break; 

      //Receive 'A' stepper rotation anti-clockwise
      case 65: 
      leftSpeed  = xbee.read();
      rightSpeed = xbee.read();

      //Set the stepper to move
      // energize coils - the motor will hold position
       digitalWrite(2,LOW); 
       stepper.setMaxSpeed(rightSpeed*100);
       //stepper.setRPM((int)rightSpeed);
       stepper.move(leftSpeed*100*(-1));
     break; 

      //Receive 'H' stepper rotation halt
      case 72: 
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("H");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      stepper.setAcceleration(1000);
      //stepper.setMaxSpeed(rightSpeed*100);
       //stepper.setRPM((int)rightSpeed);
       //stepper.move(leftSpeed);
       stepper.stop();
     break; 

  }

//--------- END EVALUATION Commands from Processing

    stepper.run(); 
    //Serial.println(stepper.distanceToGo());
      if (stepper.distanceToGo() == 0)
    {
      //DISINGAGGIA  
        digitalWrite(2,HIGH); 
    } else {
      //INGAGGIA  
        digitalWrite(2,LOW); 
      }


  
}
