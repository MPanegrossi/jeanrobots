#include <SoftwareSerial.h>

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h> 


boolean isCenter=false;
boolean isCenterUP=false;
boolean isLeft=false;
boolean isRight=false;
boolean isUp=false;
boolean isDown=false;

class Sweeper
{
  Servo servo;              // the servo
  int pos;              // current servo position 
  int increment;        // increment to move for each interval
  int  updateInterval;      // interval between updates
  unsigned long lastUpdate; // last update of position
 
public: 
  Sweeper(int interval)
  {
    updateInterval = interval;
    increment = 5;
    pos=0;
  }
  
  void Attach(int pin)
  {
    servo.attach(pin);
  }
  
  void Detach()
  {
    servo.detach();
  }

  int getPos()
  {
    return this->pos;
  }

//FUNCTIONS FOR BOTH SERVOS
  void init(int _pos)
  {
      pos = _pos; 
      servo.write(pos);

  }
  
 /* void Update()
  {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      pos += increment;
      servo.write(pos);
      //Serial.println(pos);
      if ((pos >= 180) || (pos <= 0)) // end of sweep
      {
        // reverse direction
        increment = -increment;
      }
    }
  }*/

//FUNCTIONS FOR BOTTOM SERVO -----------------------------------------------------------------------
  void Center(int centerPos)
  {
    int _centerPos=centerPos;
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(pos<_centerPos)
      {
        pos += increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }      
       if(pos>_centerPos)
      {
        pos -= increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       
       if(getPos()==_centerPos)
       {
        isCenter=false;
       }
    }
  }

  void turnLeft()
  {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()>0)
      {
        pos -= increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==0)
       {
        isLeft=false;
       }
    }
  }

  void turnRight()
  {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()<180)
      {
        pos += increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==180)
       {
        isRight=false;
       }
    }
  }

//END FUNCTIONS FOR BOTTOM SERVO -----------------------------------------------------------------------

  

//FUNCTIONS FOR UPPER SERVO -----------------------------------------------------------------------

  void CenterUP(int centerPos)
  {
    int _centerPos=centerPos;
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()<_centerPos)
      {
        pos += increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }      
       if(pos>_centerPos)
      {
        pos -= increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       
       if(getPos()==_centerPos)
       {
        isCenterUP=false;
       }
    }
  }

  void UP()
  {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()<100)
      {
        pos += increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==100)
       {
        isUp=false;
       }
    }
  }

    void DOWN()
  {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()>45)
      {
        pos -= increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==45)
       {
        isDown=false;
       }
    }
  }
  
};


//----------------MOTORS SETUP

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Connect a stepper motor with 200 steps per revolution (1.8 degree)
// to motor port #2 (M3 and M4)
Adafruit_StepperMotor *platformStepper = AFMS.getStepper(200, 2);
// And connect a DC motor to port M1
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
// And connect a DC motor to port M1
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

// We'll also test out the built in Arduino Servo library
Servo servoTilt;

//How many steps the steppers has to move
int steps = 0;



//-------------- SOFTWARESERIAL SETUP

// Define SoftSerial TX/RX pins
// Connect Arduino pin 8 to TX of usb-serial device
uint8_t ssRX = 8;
// Connect Arduino pin 9 to RX of usb-serial device
uint8_t ssTX = 9;

SoftwareSerial xbee(ssRX, ssTX);

void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200);
  xbee.begin(115200);

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Attach a servo to pin #10
  servoTilt.attach(10);
   
  // turn on motor M1
  leftMotor->setSpeed(0);
  leftMotor->run(RELEASE);

    // turn on motor M2
  rightMotor->setSpeed(0);
  rightMotor->run(RELEASE);
  
  // setup the stepper
  platformStepper->setSpeed(100);  // 10 rpm   

}

void loop() {

  //Receive Bytes from Pocessing
  byte b = 0;
  if(xbee.available() > 0)
  {
    b = xbee.read();
    //Serial.println(b);
  }

  //---------Commands from Processing
  switch (b)
  {
    //Keyboard 'i'
    case 70: 
      Serial.println('F');
      //Connect the motors
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      //Set the speed
      leftMotor->setSpeed(100);  
      rightMotor->setSpeed(100);
      //Set the stepper to move
      steps = 1;
    break;  
    //Keyboard 'm'
    case 77: 
      Serial.println('B');
    break; 
    //Keyboard 'j'
    case 66: 
      Serial.println('L');
    break; 
    //Keyboard 'l'
    case 82: 
      Serial.println('R');
    break; 
    //Keyboard 'k'
    case 77: 
      Serial.println('S');
    break;  
    //Keyboard 'o'
    case 111: 
      Serial.println("Ready!!");
    break;  
  }

  if(steps > 0)
  {
    platformStepper->step(steps, FORWARD, DOUBLE);
  }
  if(steps < 0)
  {
    platformStepper->step(abs(steps), BACKWARD, DOUBLE);
  }
  if(steps == 0)
  {
    platformStepper->release();
  }

  servo1.write(pos);

  
}
