#include <SoftwareSerial.h>

#include <Arduino.h>
#include <AccelStepper.h>

#include <DRV8835MotorShield.h>

int minDistance = 50;

long start=0;
bool once = true;
bool twice = false;

//----------------VALUES FOR STRING PARSER

const int bufferSize = 50; // how big is the buffer
 
char buffer[bufferSize];  // Serial buffer
char commandBuffer[10];   // array to store a command
char pinBuffer[3];        // array to store a pin number 
char valueBuffer1[12];     // array to store a value
char valueBuffer2[12];     // array to store a value
char valueBuffer3[12];     // array to store a value
char valueBuffer4[12];     // array to store a value
int ByteCount;            // how many bytes arrived

boolean ledON;            // state of the LED
int pinNumber = 13;            // pinNumber
float value1;                // brightness value
float value2;                // brightness value
float value3;                // brightness value
float value4;                // brightness value

//----------------MOTORS SETUP

DRV8835MotorShield motors;
//pins 10,9,8,7,6

AccelStepper stepper(AccelStepper::DRIVER, 3, 4);

// Motor steps per revolution. Most steppers are 200 steps or 1.8 degrees/step
//#define MOTOR_STEPS 200

//SETTINGS FOR THE STEPPER MOTOR

// All the wires needed for full functionality
//#define DIR 2
//#define STEP 19
//Uncomment line to use enable/disable functionality
//#define ENBL 5

// Since microstepping is set externally, make sure this matches the selected mode
// 1=full step, 2=half step etc.
//#define MICROSTEPS 1

//Uncomment line to use enable/disable functionality
//BasicStepperDriver stepper(MOTOR_STEPS, DIR, STEP, ENBL);


//-------------- SOFTWARESERIAL SETUP
/*
//Define SoftSerial TX/RX pins
// Connect Arduino pin 8 to TX of usb-serial device
uint8_t ssRX = 11;
// Connect Arduino pin 9 to RX of usb-serial device
uint8_t ssTX = 12;

SoftwareSerial huzzah(ssRX, ssTX);*/

//----------------AUDIO BOARD SETUP



void setup() {
  // put your setup code here, to run once:

  delay(5000);

  Serial.begin(115200);
  //huzzah.begin(115200);
 

    // uncomment one or both of the following lines if your motors' directions need to be flipped
  //motors.flipM1(true);
  //motors.flipM2(true);

    pinMode(2,OUTPUT);

  /*
     * Set target motor RPM.
     * These motors can do up to about 200rpm.
     * Too high will result in a high pitched whine and the motor does not move.
     */
    //stepper.setRPM(100);
    
    motors.setM1Speed(0);
    motors.setM2Speed(0);

    
    //stepper.setMaxSpeed(0);
    stepper.setAcceleration(250);

    //DISINGAGGIA  
    digitalWrite(2,HIGH);


}

void loop() {
    
//read the data and parse it
  SerialParser();

  //if something has been parsed 
  if (ByteCount > 0) 
  {
   /* Serial.print(value1);
    Serial.print(",");
    Serial.print(value2);
    Serial.print(",");
    Serial.print(value3);
    Serial.print(",");
    Serial.println(value4);*/

    
  //Call different functions according to the label
    if (strcmp(commandBuffer, "wheels") == 0)
    {
      motors.setM1Speed(value1*400*value3);
      motors.setM2Speed(value2*400*value3);
    }

    if (strcmp(commandBuffer, "step") == 0)
    {
      //INGAGGIA  
      digitalWrite(2,LOW);
     stepper.setAcceleration(value1);
     stepper.setMaxSpeed(value2);
     //stepper.setRPM((int)rightSpeed);
     stepper.move(value3);
    }

    if (strcmp(commandBuffer, "stepTo") == 0)
    {
      //INGAGGIA  
      digitalWrite(2,LOW);
     stepper.setAcceleration(value1);
     stepper.setMaxSpeed(value2);
     //stepper.setRPM((int)rightSpeed);
     stepper.moveTo(value3);
    }

    if (strcmp(commandBuffer, "stepStop") == 0)
    {
    stepper.setAcceleration(value1);
    //stepper.setMaxSpeed(rightSpeed*100);
     //stepper.setRPM((int)rightSpeed);
     //stepper.move(leftSpeed);
     //stepper.move(0);
     stepper.stop();
     //DISINGAGGIA  
     //digitalWrite(5,HIGH); 
     once = true;
    }
  }
  
    //--------- END EVALUATION Commands from Processing

    stepper.run(); 
    //Serial.println(stepper.distanceToGo());

        
        if(stepper.distanceToGo() == 0)
        {
          if(once)
          {
            start = millis();
            once = false;
            twice = true;
            //stepper.stop();
          }
        }

        if(millis() - start > 500 && twice)
        {   
          //DISGAGGIA  
        digitalWrite(2,HIGH); 
        twice = false;
        }
        
  
}

void SerialParser() 
{
  ByteCount = -1;
  
  // if something has arrived over serial port
  if (Serial.available() > 0)
  {
    //read the first character
    char ch = Serial.read();
    
    //if it's 's', then it's the start of the message
    if (ch == 's')
    {
      //read all bytes of the message until the newline character ('\n')
       ByteCount =  Serial.readBytesUntil('\n',buffer,bufferSize); 
       
       //if the number of arrived bytes > 0
       if (ByteCount > 0) 
       {
            // copy the string until the first ','
            strcpy(commandBuffer, strtok(buffer, ","));
            
            // copy the same string until the next ','
           // strcpy(pinBuffer, strtok(NULL, ","));
     
            // copy the same string until the next ',' 
            strcpy(valueBuffer1, strtok(NULL, ","));

            // copy the same string until the next ',' 
            strcpy(valueBuffer2, strtok(NULL, ","));
            // copy the same string until the next ',' 
            strcpy(valueBuffer3, strtok(NULL, ","));
            // copy the same string until the next ',' 
            strcpy(valueBuffer4, strtok(NULL, ","));
            
            //check the documentation about strtok() at:
            //http://www.gnu.org/software/libc/manual/html_node/Finding-Tokens-in-a-String.html
         
            //check the arrived command and set the LED state
            //this is how to compare two char arrays (simple strings)
            //if they are equal, strcmp returns 0
         /*   if (strcmp(commandBuffer, "LED_ON") == 0)
            {
              ledON = true;
            }
            else
            {
              ledON = false;
            }*/
            
            // convert the string into an 'int' value
          //  pinNumber = atoi (pinBuffer);   
   
            // convert the sting into a 'float' value and bring it to (0..255) range;
            value1 = atof(valueBuffer1); 

            // convert the sting into a 'float' value and bring it to (0..255) range;
            value2 = atof(valueBuffer2); 

            // convert the sting into a 'float' value and bring it to (0..255) range;
            value3 = atof(valueBuffer3); 

            // convert the sting into a 'float' value and bring it to (0..255) range;
            value4 = atof(valueBuffer4);  

            //Serial.println("END OF PARSER");
       }
       
       // clear contents of buffer
       memset(buffer, 0, sizeof(buffer));   
       Serial.flush();
    }
  }
}
