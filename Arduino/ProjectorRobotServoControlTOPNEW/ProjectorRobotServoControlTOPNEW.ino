#include <Servo.h> 

boolean isCenter=false;
boolean isCenterUP=false;
boolean isLeft=false;
boolean isRight=false;
boolean isUp=false;
boolean isDown=false;

byte leftSpeed;
byte rightSpeed;

unsigned long lastUpdate=0; 
unsigned long updateInterval=0; 

int servoDestination = 83; //where to tilt the servo with every OSC command

class Sweeper
{
  Servo servo;               // the servo
  int pos;                   // current servo position 
  int increment;             // increment to move for each interval
  int  updateInterval;       // interval between updates
  unsigned long lastUpdate;  // last update of position
 
public: 
  Sweeper(int interval)
  {
    updateInterval = interval;
    increment = 1;
    pos=0;
  }
  
  void Attach(int pin)
  {
    servo.attach(pin);
  }
  
  void Detach()
  {
    servo.detach();
  }

  int getPos()
  {
    return this->pos;
  }

//FUNCTIONS FOR BOTH SERVOS
  void init(int _pos)
  {
      pos = _pos; 
      servo.write(pos);

  }
  

//FUNCTIONS FOR BOTTOM SERVO -----------------------------------------------------------------------
  void Center(int centerPos)
  {
    int _centerPos=centerPos;
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(pos<_centerPos)
      {
        pos += increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }      
       if(pos>_centerPos)
      {
        pos -= increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       
       if(getPos()==_centerPos)
       {
        isCenter=false;
       }
    }
  }

  void turnLeft()
  {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()>0)
      {
        pos -= increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==0)
       {
        isLeft=false;
       }
    }
  }

  void turnRight()
  {
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()<180)
      {
        pos += increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==180)
       {
        isRight=false;
       }
    }
  }

//END FUNCTIONS FOR BOTTOM SERVO -----------------------------------------------------------------------

  

//FUNCTIONS FOR UPPER SERVO -----------------------------------------------------------------------

  void CenterUP(int centerPos)
  {
    digitalWrite(13,HIGH);
    int _centerPos=centerPos;
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()<_centerPos)
      {
        pos += increment;
        servo.write(getPos());
        /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }      
       if(pos>_centerPos)
      {
        pos -= increment;
        servo.write(getPos());
      /*  if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       
       if(getPos()==_centerPos)
       {
        isCenterUP=false;
        digitalWrite(13,LOW);
       }
    }
  }

  void UP()
  {
    digitalWrite(13,HIGH);
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()>servoDestination)
      {
        pos -= increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==servoDestination)
       {
        isUp=false;
        digitalWrite(13,LOW);
       }
    }
  }

  void DOWN()
  {
    digitalWrite(13,HIGH);
    if((millis() - lastUpdate) > updateInterval)  // time to update
    {
      lastUpdate = millis();
      if(getPos()<servoDestination)
      {
        pos += increment;
        servo.write(getPos());
    /*   if ((pos >= 180) || (pos <= 0)) // end of sweep
        {
          // reverse direction
          increment = -increment;
        }*/
       }
       if(getPos()==servoDestination)
       {
        isDown=false;
        digitalWrite(13,LOW);
       }
    }
  }
  
};


//----------------MOTORS SETUP

Sweeper servoTilt(50);


void setup() {
  // put your setup code here, to run once:

  Serial.begin(57600);
  
  // Attach a servo to pin #10
  servoTilt.Attach(10);

  //Set the servo to its center position
  servoTilt.init(83);

  pinMode(13,OUTPUT);
  digitalWrite(13,LOW);

}

void loop() {

  //Receive Bytes from Pocessing
  byte b = 0;
  if(Serial.available() > 2)
  {
    b = Serial.read();
    //Serial.println(b);
  }

  //--------- EVALUATION Commands from Processing
  switch (b)
  {

    case 84: 
    //TILTER
      leftSpeed  = Serial.read();
      rightSpeed = Serial.read();
      Serial.println("T");
      Serial.println(leftSpeed);
      Serial.println(rightSpeed);
      if(rightSpeed == 1)
      {
        isDown = true;
        servoDestination = leftSpeed;
      }
      if(rightSpeed == 2)
      {
        isUp = true;
        servoDestination = leftSpeed;
      }
      if(rightSpeed == 0)
      {
        isCenterUP = true;
      }

    break; 

  }

//--------- END EVALUATION Commands from Processing

//MOVE THE SERVO
 if(isCenter || isCenterUP)
  {
      isLeft=false;
      isRight=false;
      isUp=false;
      isDown=false;
      servoTilt.CenterUP(83);
      //Serial.println(sweeper2.getPos());
  }

  if(isUp)
  {
    isCenter=false;
    isCenterUP=false;
    isLeft=false;
    isRight=false;
    isDown=false;
    servoTilt.UP();
    
   // Serial.println(sweeper1.getPos());
  }
  
  if(isDown)
  {
    isCenter=false;
    isCenterUP=false;
    isLeft=false;
    isRight=false;
    isUp=false;
    servoTilt.DOWN();
   // Serial.println(sweeper1.getPos());
  }
    
}


