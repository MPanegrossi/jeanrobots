

#include <Arduino.h>
#include <AccelStepper.h>

#include <DRV8835MotorShield.h>

#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>

int minDistance = 50;

long start=0;
bool once = true;
bool twice = false;

//----------------VALUES FOR STRING PARSER

const int bufferSize = 50; // how big is the buffer
 
char buffer[bufferSize];  // Serial buffer
char commandBuffer[10];   // array to store a command
char pinBuffer[3];        // array to store a pin number 
char valueBuffer1[12];     // array to store a value
char valueBuffer2[12];     // array to store a value
char valueBuffer3[12];     // array to store a value
char valueBuffer4[12];     // array to store a value
int ByteCount;            // how many bytes arrived

boolean ledON;            // state of the LED
int pinNumber = 13;            // pinNumber
float value1;                // brightness value
float value2;                // brightness value
float value3;                // brightness value
float value4;                // brightness value

boolean runToSpeedEnable = false;

int lastValue;
int valueToAdd;

//----------------MOTORS SETUP

DRV8835MotorShield motors;
//pins 10,9,8,7,6

AccelStepper stepper(AccelStepper::DRIVER, 19, 2);

// Motor steps per revolution. Most steppers are 200 steps or 1.8 degrees/step
//#define MOTOR_STEPS 200

//SETTINGS FOR THE STEPPER MOTOR

// All the wires needed for full functionality
//#define DIR 2
//#define STEP 19
//Uncomment line to use enable/disable functionality
//#define ENBL 5

// Since microstepping is set externally, make sure this matches the selected mode
// 1=full step, 2=half step etc.
//#define MICROSTEPS 1

//Uncomment line to use enable/disable functionality
//BasicStepperDriver stepper(MOTOR_STEPS, DIR, STEP, ENBL);


//-------------- SOFTWARESERIAL SETUP
/*
// Define SoftSerial TX/RX pins
// Connect Arduino pin 8 to TX of usb-serial device
uint8_t ssRX = 2;
// Connect Arduino pin 9 to RX of usb-serial device
uint8_t ssTX = 19;*/

//SoftwareSerial Serial(ssRX, ssTX);

//----------------AUDIO BOARD SETUP

// These are the pins used for the breakout example
#define BREAKOUT_RESET  15      // VS1053 reset pin (output)
#define BREAKOUT_CS     16     // VS1053 chip select pin (output)
#define BREAKOUT_DCS    17      // VS1053 Data/command select pin (output)


// These are common pins between breakout and shield
#define CARDCS 4     // Card chip select pin
// DREQ should be an Int pin, see http://arduino.cc/en/Reference/attachInterrupt
#define DREQ 3       // VS1053 Data request, ideally an Interrupt pin

Adafruit_VS1053_FilePlayer musicPlayer = 
  // create breakout-example object!
  Adafruit_VS1053_FilePlayer(BREAKOUT_RESET, BREAKOUT_CS, BREAKOUT_DCS, DREQ, CARDCS);
  // create shield-example object!
  //Adafruit_VS1053_FilePlayer(SHIELD_RESET, SHIELD_CS, SHIELD_DCS, DREQ, CARDCS);

  String tracks[] = {"track002.mp3","blask.mp3","lagrima.mp3","unter.mp3","somew.mp3","scrat.mp3","goodbye.mp3","hello.mp3","no.mp3","ok.mp3","yes.mp3","speech1.mp3","speech2.mp3","anchor.mp3","soprexp.mp3"};

//-----------END

void setup() {
  // put your setup code here, to run once:

  delay(5000);

  Serial.begin(115200);
  //Serial.begin(9600);

  pinMode(5,OUTPUT);
 

    // uncomment one or both of the following lines if your motors' directions need to be flipped
  //motors.flipM1(true);
  //motors.flipM2(true);

  /*
     * Set target motor RPM.
     * These motors can do up to about 200rpm.
     * Too high will result in a high pitched whine and the motor does not move.
     */
    //stepper.setRPM(100);
    
    motors.setM1Speed(0);
    motors.setM2Speed(0);

    
    //stepper.setMaxSpeed(0);
    stepper.setAcceleration(250);
    //DISINGAGGIA  
    digitalWrite(5,HIGH);

  
  if (! musicPlayer.begin()) { // initialise the music player
     Serial.println(F("Couldn't find VS1053, do you have the right pins defined?"));
     while (1);
  }
  //Serial.println(F("VS1053 found"));
  
  SD.begin(CARDCS);    // initialise the SD card
  
  // Set volume for left, right channels. lower numbers == louder volume!
  musicPlayer.setVolume(0,0);

  // Timer interrupts are not suggested, better to use DREQ interrupt!
  //musicPlayer.useInterrupt(VS1053_FILEPLAYER_TIMER0_INT); // timer int

  // If DREQ is on an interrupt pin (on uno, #2 or #3) we can do background
  // audio playing
  musicPlayer.useInterrupt(VS1053_FILEPLAYER_PIN_INT);  // DREQ int
  
  // Play one file, don't return until complete
  //Serial.println(F("Playing track 001"));
  //musicPlayer.playFullFile("track001.mp3");

 pinMode(15,OUTPUT);
 pinMode(16,OUTPUT);
 pinMode(17,OUTPUT);
 pinMode(18,OUTPUT);
 pinMode(19,OUTPUT);

}

void loop() {

   //----- IF ROBOT TOO CLOSE TO AN OBSTACLE THEN STOP
  int sensorValue = analogRead(A0);
  //Serial.println(sensorValue);
  if(sensorValue < 0)
  {
      motors.setM1Speed(0);
      motors.setM2Speed(0);
      
  }

  //read the data and parse it
  SerialParser();

  //if something has been parsed 
  if (ByteCount > 0) 
  {
   /* Serial.print(value1);
    Serial.print(",");
    Serial.print(value2);
    Serial.print(",");
    Serial.print(value3);
    Serial.print(",");
    Serial.println(value4);*/

    
  //Call different functions according to the label
    if (strcmp(commandBuffer, "wheels") == 0)
    {
      motors.setM1Speed(value1*400*value3);
      motors.setM2Speed(value2*400*value3);
    }

    if (strcmp(commandBuffer, "step") == 0)
    {
      //INGAGGIA  
      digitalWrite(5,LOW);
     stepper.setAcceleration(value1);
     stepper.setMaxSpeed(value2);
     //stepper.setRPM((int)rightSpeed);
     stepper.move(value3);
    }

    if (strcmp(commandBuffer, "stepTo") == 0)
    {
      //INGAGGIA  
      digitalWrite(5,LOW);
      //Serial.println("Ho Ingaggiato");
     stepper.setAcceleration(value1);
     stepper.setMaxSpeed(value2);
     //stepper.setRPM((int)rightSpeed);
     int valueint = value3;
     
     Serial.println("Messaggio");
     Serial.println(stepper.distanceToGo()); 
     once = true;
     
     if(lastValue - valueint > 190)
     {
      valueToAdd += 200;
     }
     if(valueint - lastValue > 190)
     {
      valueToAdd -= 200;
     }

     int valueToMove = valueint+valueToAdd;
     stepper.moveTo(valueToMove); 
     lastValue = valueint;
    }

    if (strcmp(commandBuffer, "stepStop") == 0)
    {
    stepper.setAcceleration(value1);
    //stepper.setMaxSpeed(rightSpeed*100);
     //stepper.setRPM((int)rightSpeed);
     //stepper.move(leftSpeed);
     //stepper.move(0);
     stepper.stop();
     //DISINGAGGIA  
     //digitalWrite(5,HIGH); 
     once = true;
    }

    if (strcmp(commandBuffer, "play") == 0)
    {

      //Play another file in the background, REQUIRES interrupts!
      //Serial.println(F("Playing track 002"));
      char track_name[50];
      tracks[(int)value1].toCharArray(track_name, 50);
      musicPlayer.startPlayingFile(track_name);
    }

    if (strcmp(commandBuffer, "stop") == 0)
    {
      musicPlayer.stopPlaying();
    }

    if (strcmp(commandBuffer, "pause") == 0)
    {
        if (! musicPlayer.paused()) {
        //Serial.println("Paused");
        musicPlayer.pausePlaying(true);
      } else { 
        //Serial.println("Resumed");
        musicPlayer.pausePlaying(false);
      }
    } 
  }
  
    //--------- END EVALUATION Commands from Processing
    Serial.println("Prima");
    Serial.println(stepper.distanceToGo());
    stepper.run(); 
    Serial.println("Dopo");
    Serial.println(stepper.distanceToGo());
    

            
        if(stepper.distanceToGo() == 0)
        {
          if(once)
          {
             start = millis();
             once = false;
          }

          if(millis() - start > 200)
          {
              //DISINGAGGIA  
              digitalWrite(5,HIGH);
              once = true;
          }
        }
  



        
        
     /* if(stepper.distanceToGo() == 0)
        {
          
          if(once)
          {
            start = millis();
            once = false;
            twice = true;
            //stepper.stop();
            Serial.println(stepper.distanceToGo());
            Serial.println("once");
          }
        }

        if(millis() - start > 500 && twice)
        {   
          //DISINGAGGIA  
        digitalWrite(5,HIGH); 
        twice = false;
        Serial.println("Disingaggiato");
        }*/
        
  
}

void SerialParser() 
{
  ByteCount = -1;
  
  // if something has arrived over serial port
  if (Serial.available() > 0)
  {
    //read the first character
    char ch = Serial.read();
    
    //if it's 's', then it's the start of the message
    if (ch == 's')
    {
      //read all bytes of the message until the newline character ('\n')
       ByteCount =  Serial.readBytesUntil('\n',buffer,bufferSize); 
       
       //if the number of arrived bytes > 0
       if (ByteCount > 0) 
       {
            // copy the string until the first ','
            strcpy(commandBuffer, strtok(buffer, ","));
            
            // copy the same string until the next ','
           // strcpy(pinBuffer, strtok(NULL, ","));
     
            // copy the same string until the next ',' 
            strcpy(valueBuffer1, strtok(NULL, ","));

            // copy the same string until the next ',' 
            strcpy(valueBuffer2, strtok(NULL, ","));
            // copy the same string until the next ',' 
            strcpy(valueBuffer3, strtok(NULL, ","));
            // copy the same string until the next ',' 
            strcpy(valueBuffer4, strtok(NULL, ","));
            
            //check the documentation about strtok() at:
            //http://www.gnu.org/software/libc/manual/html_node/Finding-Tokens-in-a-String.html
         
            //check the arrived command and set the LED state
            //this is how to compare two char arrays (simple strings)
            //if they are equal, strcmp returns 0
         /*   if (strcmp(commandBuffer, "LED_ON") == 0)
            {
              ledON = true;
            }
            else
            {
              ledON = false;
            }*/
            
            // convert the string into an 'int' value
          //  pinNumber = atoi (pinBuffer);   
   
            // convert the sting into a 'float' value and bring it to (0..255) range;
            value1 = atof(valueBuffer1); 

            // convert the sting into a 'float' value and bring it to (0..255) range;
            value2 = atof(valueBuffer2); 

            // convert the sting into a 'float' value and bring it to (0..255) range;
            value3 = atof(valueBuffer3); 

            // convert the sting into a 'float' value and bring it to (0..255) range;
            value4 = atof(valueBuffer4);  

            //Serial.println("END OF PARSER");
       }
       
       // clear contents of buffer
       memset(buffer, 0, sizeof(buffer));   
       Serial.flush();
    }
  }
}
