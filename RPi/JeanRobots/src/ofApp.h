#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxOMXPlayer.h"

#include "ofAppEGLWindow.h"
#include "TerminalListener.h"
#include "ofxRPiCameraVideoGrabber.h"
#include "ImageFilterCollection.h"


#define PORT 9000
#define sPORT 53000
#define HOST "192.168.1.111"

class ofApp : public ofBaseApp, public ofxOMXPlayerListener, public KeyListener{
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    //user defined
    void exit();
    void mediaChooser(int imageNumber);
    
    ofxOscReceiver receive;
    ofxOscSender sender;
    float oscX = 0.0;
    float oscY = 0.0;
    
    ofxOMXPlayer omxPlayer;
    
    void onVideoEnd(ofxOMXPlayerListenerEventData& e);
    void onVideoLoop(ofxOMXPlayerListenerEventData& e){ /*empty*/ };
    
    ofxOMXPlayerSettings settings;
    
    string state_button;
    bool once;
    bool doPlay;
    int counter;
    
    ofSerial	serial;
    
    bool play;
    bool scanner;
    int scannerTransparency;
    int fadingDirection; //1 for up and 0 for down
    int lineX1,lineX2, lineY1, lineY2;
    int linePosition;
    vector<int> linePositions;
    
    int r;
    int g;
    int b;
    
    ofImage mask;
    ofBlendMode blendMode;
    
    ofImage reel;
    bool image;
    
    void onCharacterReceived(KeyListenerEventData& e);
    TerminalListener consoleListener;
    ofxRPiCameraVideoGrabber videoGrabber;
    
    ImageFilterCollection filterCollection;
    
    bool doDrawInfo;
    
    ofFbo fbo;
    ofShader shader;
    bool doShader;
    bool camera = false;
    
    OMXCameraSettings omxCameraSettings;
    
};
