#include "ofApp.h"

string videoPath1 = ofToDataPath("movies/kent1Feather.mp4",true);
string videoPath2 = ofToDataPath("movies/kent2.mp4",true);
string cameraFX[25] = {"None","Noise","Emboss","Negative","Sketch","OilPaint","Hatch","Gpen","Antialias","DeRing","Solarize","Watecolor","Pastel","Sharpen","Film","Blur","Saturation","DeInterlaceLineDouble","DeInterlaceAdvanced","ColourSwap","WashedOut","ColourPoint","Posterise","ColourBalance","Cartoon"};

void ofApp::onVideoEnd(ofxOMXPlayerListenerEventData& e)
{
    ofLogVerbose(__func__) << "END OF VIDEO";
    
	//SERIAL MESSAGE OUT
	/*
    serial.writeByte('e');
	*/
    
    //Informs QLab that the video has finished
    ofxOscMessage s;
    s.setAddress( "/videoEnd");
    s.addIntArg(1);
    //s.addFloatArg( 3.5f );
    //s.addStringArg( "hello" );
    //s.addFloatArg( ofGetElapsedTimef() );
	/*
	//SEND SERIAL MESSAGE OUT
    sender.sendMessage( s );
	*/
    
    play=false;
}

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetFrameRate(30);
    ofBackground(0,0,0);
    ofSetLogLevel(OF_LOG_VERBOSE);
    //new stuff
    ofSetLogLevel("ofThread", OF_LOG_ERROR);
    ofSetVerticalSync(true);
    //
    ofHideCursor();
    
    ofBackground(0,0,0);
    
    mask.allocate(1280, 720, OF_IMAGE_COLOR_ALPHA);
    
    mask.load("movies/vignetteMask.png");
    
    //reel.load("movies/image1.jpg");
    
    blendMode = OF_BLENDMODE_ALPHA;
    
    play = false;
    camera=false;
    image=false;
    scanner = false;
    scannerTransparency = 255;
    fadingDirection = 0;
    lineX1=0;
    lineX2=0;
    lineY1=0;
    lineY2=720;
    
    //SETUP FOR VIDEO PLAYER
    settings.videoPath = videoPath1;
    settings.useHDMIForAudio = true;
    settings.enableTexture = true;
    settings.enableLooping = false;
    settings.enableAudio = false;
    settings.listener = this;
    
    // open an incoming connection on PORT
    receive.setup(PORT);
    // open an outgoing connection to HOST:PORT
    sender.setup( HOST, sPORT );
    
    counter = 0;
    
    //START THE SERIAL COMMUNICATION
	/*
    serial.listDevices();
    vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
    
    // this should be set to whatever com port your serial device is connected to.
    // (ie, COM4 on a pc, /dev/tty.... on linux, /dev/tty... on a mac)
    // arduino users check in arduino app....
    int baud = 57600;
    //serial.setup(0, baud); //open the first device
    
    //THIS IS WHAT WAS BEFORE
    //serial.setup("/dev/ttyACM0", baud); // windows example
    
    //THIS IS THE NEW ONE FOR JEAN
    serial.setup("/dev/ttyAMA0", baud); // windows example
    
    
    //serial.setup("/dev/tty.usbserial-A4001JEC", baud); // mac osx example
    //serial.setup("/dev/ttyUSB0", baud); //linux example
	*/
    
    //SETUP FOR THE CAMERA
    //allows keys to be entered via terminal remotely (ssh)
    consoleListener.setup(this);
    
    omxCameraSettings.width = 1280; //default 1280
    omxCameraSettings.height = 720; //default 720
    omxCameraSettings.enableTexture = true; //default true
    omxCameraSettings.doRecording = false;   //default false
    videoGrabber.setup(omxCameraSettings);
    
    if (omxCameraSettings.doRecording)
    {
        omxCameraSettings.recordingFilePath = ""; //default "" will self-generate
        
    }
    
    //CAMERA ROATION SETTING
    videoGrabber.setRotation(ROTATION_180);
    
    //ImageFilterCollection (filterCollection here) is helper class to iterate through available OpenMax filters
    filterCollection.setup();
    
    linePosition=0;
    linePositions.push_back(linePosition);
    
}

//--------------------------------------------------------------
void ofApp::update(){
    
    if(scanner)
    {
      /*  if(scannerTransparency > 0 && fadingDirection == 0)
        {
            scannerTransparency--;
        }
        
        if(scannerTransparency <= 255 && fadingDirection == 1)
        {
            scannerTransparency++;
        }
        
        if(scannerTransparency = 1)
        {
            scannerTransparency++;
            fadingDirection = 1;
        }*/

    }
    
    while(receive.hasWaitingMessages())
    {
        ofxOscMessage m;
        receive.getNextMessage(m);
        
      /*  if(m.getAddress() == "/start")
        {
            //oscY=m.getArgAsFloat(0);
            //ofClear(0,0,0);
            
            if(counter == 0)
            {
                //omxPlayer.setup(settings);
                play=true;
                image = false;
                camera = false;
                //serial.writeByte('s');
                counter = 1;
            }
            if(counter > 0)
            {
                play=true;
                image = false;
                camera = false;
                omxPlayer.restartMovie();
                //serial.writeByte('s');
            }
            
        }*/
        
        //DRAW THE CAMERA
        if(m.getAddress() == "/camera")
        {
            if(camera == false)
            {
                camera = true;
                play=false;
                image = false;
            }

            
        }
        
        //CHOOSE THE CAMERA FX. FX LIST AT THE END OF FILE
        if(m.getAddress() == "/cameraFX")
        {
            int fxInt = m.getArgAsInt(0);
            if(camera == true)
            {
                videoGrabber.setImageFilter(cameraFX[fxInt]);
            }
        }
        
       /* if(m.getAddress() == "/pi/tilt")
        {
            int posArg = m.getArgAsInt(0);
            int voidArg = m.getArgAsInt(1);
            serial.writeByte('T');
            serial.writeByte(posArg);
            serial.writeByte(voidArg);
            
        }*/
        
        //CHANGE THE BACKGROUND COLOR
        if(m.getAddress() == "/background")
        {
            r = m.getArgAsInt(0);
            g = m.getArgAsInt(1);
            b = m.getArgAsInt(2);
        }
    
        //LOAD AN IMAGE OR A VIDEO
        if(m.getAddress() == "/image")
        {
            int value = m.getArgAsInt(0);

			mediaChooser(value);
            
        }
        
        //turn on the scanner
        if(m.getAddress() == "/scanner")
        {
            scanner = true;
            
        }
        
        //SHUTDOWN
        if(m.getAddress() == "/shutdown")
        {
            //oscY=m.getArgAsFloat(0);
            
            ofSystem("sudo shutdown -h now");
            
        }
        
        //EXIT THE PROGRAM
        if(m.getAddress() == "/exit")
        {
            //oscY=m.getArgAsFloat(0);
            
            exit();
            
        }
        
    }
    if(scanner)
    {   if(linePositions.size()<40)
        {
            linePositions.push_back(linePosition+8);
        }
        if(linePositions.size() == 40)
        {
            for(int j=0; j<40;j++)
            {
                linePositions[j]=linePositions[j]+8;
            }
        }
        
    }
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    if(camera)
    {
        //draws at camera resolution
        videoGrabber.draw();
        
		/*
        //draw a smaller version via the getTextureReference() method
        int drawWidth = omxCameraSettings.width/4;
        int drawHeight = omxCameraSettings.height/4;
        videoGrabber.getTextureReference().draw(omxCameraSettings.width-drawWidth, omxCameraSettings.height-drawHeight, drawWidth, drawHeight);
		*/
        
        stringstream info;
        info << "App FPS: " << ofGetFrameRate() << "\n";
        info << "Camera Resolution: " << videoGrabber.getWidth() << "x" << videoGrabber.getHeight()	<< " @ "<< videoGrabber.getFrameRate() <<"FPS"<< "\n";
        info << "CURRENT FILTER: " << filterCollection.getCurrentFilterName() << "\n";
        //info <<	filterCollection.filterList << "\n";
        
        info << "\n";
        info << "Press e to increment filter" << "\n";
        info << "Press g to Toggle info" << "\n";
        if (omxCameraSettings.doRecording)
        {
            info << "Press q to stop recording" << "\n";
        }
        
        if (doDrawInfo)
        {
            ofDrawBitmapStringHighlight(info.str(), 100, 100, ofColor::black, ofColor::yellow);
        }
    }
    
    
    if(play){
        omxPlayer.draw(0, 0, 1280, 720);
    }
    if((!play) && (!camera) && (!image))
    {
        ofBackground(r,g,b);
    }
    if(image)
    {
        reel.draw(0,0,1280,720);
    }
    if(scanner)
    {
       /* ofEnableAlphaBlending();
        ofSetLineWidth(8.0f);

            ofSetColor(0,0,255,scannerTransparency);
            ofDrawLine(lineX1+j,lineY1,lineX2+j,lineY2);
            if(j<1280)
            {
                j+=5;
            }
            if(j>=1280)
            {
                j-=5;
            }
        if(scannerTransparency == 0)
        {
            scannerTransparency = 255;
        }
        scannerTransparency--;
        
        ofDisableAlphaBlending();*/
        
        
        for(int n =0;n<linePositions.size();n++)
        {
            
            ofSetLineWidth(8.0f);
            scannerTransparency = 255-(n*6);
            
            ofEnableAlphaBlending();
            ofSetColor(0,0,255,scannerTransparency);
            ofDrawLine(linePositions[n],0,linePositions[n],720);
            ofDisableAlphaBlending();
            
        }
        
        
        
        
    }
    
    ofEnableBlendMode(blendMode);
    
    mask.draw(0, 0,1280,720);
    
    ofDisableBlendMode();
    
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key)
{
    ofLog(OF_LOG_VERBOSE, "%c keyPressed", key);
    
    if (key == 'e')
    {
        videoGrabber.setImageFilter(filterCollection.getNextFilter());
    }
    
    if (key == 'g')
    {
        doDrawInfo = !doDrawInfo;
    }
    
    if (key == 's')
    {
        doShader = !doShader;
    }
    
}

void ofApp::onCharacterReceived(KeyListenerEventData& e)
{
    keyPressed((int)e.character);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){
    
}

void ofApp::exit(){
    omxPlayer.close();
    //serial.close();
    videoGrabber.close();
    ofShowCursor();
    //ofExit();
    std::exit(0);
}

void ofApp::mediaChooser(int imageNumber)
{
            switch(imageNumber)
            {
                case 0:
                    image = false;
                    reel.clear();
                    break;
                case 1:
                    reel.load("movies/image1.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 2:
                    reel.load("movies/image2.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 3:
                    reel.load("movies/image3.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 4:
                    reel.load("movies/image4.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 5:
                    reel.load("movies/image5.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 6:
                    reel.load("movies/image6.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 7:
                    reel.load("movies/image7.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 8:
                    reel.load("movies/image8.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 9:
                    reel.load("movies/image9.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 10:
                    reel.load("movies/image10.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 11:
                    reel.load("movies/image11.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 12:
                    reel.load("movies/image12.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 13:
                    reel.load("movies/image13.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 14:
                    reel.load("movies/image14.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 15:
                    reel.load("movies/image15.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 16:
                    reel.load("movies/image16.jpg");
                    image = true;
                    play=false;
                    camera=false;
                    break;
                case 17:
                    image=false;
                    camera=false;
                    play=true;
                    
                    settings.videoPath = videoPath1;
                    omxPlayer.setup(settings);
                    omxPlayer.setFullScreen(true);
                    
                    break;
                case 18:
                    image=false;
                    camera=false;
                    play=true;
                    
                    settings.videoPath = videoPath2;
                    omxPlayer.setup(settings);
                    omxPlayer.setFullScreen(true);
                    
                    break;
                default:
                    break;
                    
            }

}

/*
 imageFilters["None"] = OMX_ImageFilterNone;
 imageFilters["Noise"] = OMX_ImageFilterNoise;
 imageFilters["Emboss"] = OMX_ImageFilterEmboss;
 imageFilters["Negative"] = OMX_ImageFilterNegative;
 imageFilters["Sketch"] = OMX_ImageFilterSketch;
 imageFilters["OilPaint"] = OMX_ImageFilterOilPaint;
 imageFilters["Hatch"] = OMX_ImageFilterHatch;
 imageFilters["Gpen"] = OMX_ImageFilterGpen;
 imageFilters["Antialias"] = OMX_ImageFilterAntialias;
 imageFilters["DeRing"] = OMX_ImageFilterDeRing;
 imageFilters["Solarize"] = OMX_ImageFilterSolarize;
 imageFilters["Watercolor"] = OMX_ImageFilterWatercolor;
 imageFilters["Pastel"] = OMX_ImageFilterPastel;
 imageFilters["Sharpen"] = OMX_ImageFilterSharpen;
 imageFilters["Film"] = OMX_ImageFilterFilm;
 imageFilters["Blur"] = OMX_ImageFilterBlur;
 imageFilters["Saturation"] = OMX_ImageFilterSaturation;
 imageFilters["DeInterlaceLineDouble"] = OMX_ImageFilterDeInterlaceLineDouble;
 imageFilters["DeInterlaceAdvanced"] = OMX_ImageFilterDeInterlaceAdvanced;
 imageFilters["ColourSwap"] = OMX_ImageFilterColourSwap;
 imageFilters["WashedOut"] = OMX_ImageFilterWashedOut;
 imageFilters["ColourPoint"] = OMX_ImageFilterColourPoint;
 imageFilters["Posterise"] = OMX_ImageFilterPosterise;
 imageFilters["ColourBalance"] = OMX_ImageFilterColourBalance;
 imageFilters["Cartoon"] = OMX_ImageFilterCartoon;
 */
