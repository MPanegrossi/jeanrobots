import processing.serial.*;
import oscP5.*;
import netP5.*;

//OSC objects
OscP5 oscP5;
NetAddress Qlab;

// Your Serial Port
    Serial xbee;

    public void setup() {
        size(50, 50);
        println("Available serial ports:");
        println(Serial.list());
        
        //Set the Coordinator Serial connection
        xbee = new Serial(this, "/dev/tty.usbserial-DA014CTE", 115200);
        
        //Send a series of bytes to the router set as the destination address of the coordinator
        xbee.write("Hello!");
        
        /* start oscP5, listening for incoming messages at port 9000 */
        oscP5 = new OscP5(this,9000);
        
        /* Send responses to QLab if necessary */
        Qlab = new NetAddress("127.0.0.1",53000);

    }
    
    

    public void draw() {
        background(0);
        
        
//-----------------Commands to set a new destination address and send 2 bytes (a char and an int);

//THE 4 routers are:
//xbee.write("ATDL40B87B80\r"); PCB antenna 1 light
//xbee.write("ATDL40B87B4F\r"); PCB antenna 2
//xbee.write("ATDL40BB71E0\r"); RPSMA antenna 1 light
//xbee.write("ATDL40BE6461\r"); RPSMA antenna 2
       
       /* for(int i=0; i<10; i++)
        {
          xbee.clear();
           xbee.write('+');
           xbee.write('+');
           xbee.write('+');
           delay(20);
           xbee.write("ATDL40B87B80\r");
           delay(10);
           xbee.write("ATCN\r");
           delay(20);
           xbee.write('A');
           xbee.write(i); 
           delay(10);
           xbee.clear();
        }
        
        for(int i=0; i<255; i++)
        {
           xbee.write('A');
           xbee.write(i); 
        }
        
        for(int i=0; i<10; i++)
        {
          xbee.clear();
           xbee.write('+');
           xbee.write('+');
           xbee.write('+');
           delay(20);
           xbee.write("ATDL40B7BDE2\r");
           delay(10);
           xbee.write("ATCN\r");
           delay(20);
           xbee.write('A');
           xbee.write(i); 
           delay(10);
           xbee.clear();
        }*/
    }
 
//Using i,k,j,l as the arrows for robot 1
public void keyPressed() {

switch (key) {

   case 'i': 
     //LIGHT FORWARD
     //Enter AT mode
           /*xbee.write('+'); xbee.write('+'); xbee.write('+');
           delay(20);
           //Set destination address
           xbee.write("ATDL40BB71E0\r");
           delay(10);
           //Exit AT mode
           xbee.write("ATCN\r");
           delay(20);*/
           //Send the direction and speed command
           xbee.write('F');
           xbee.write('c');
           xbee.write('d');
   break;  
   
   case 'k': 
     xbee.write('S');
     xbee.write(0);
     xbee.write(0);
   break; 
   
   case 'j': 
     xbee.write('L');
   break; 
   
   case 'l': 
     xbee.write('R');
   break; 
   
   case 'm': 
     xbee.write('B');
   break; 
   
   
  /* case 'b': 
   xbee.write("ATDL40B87B80\r");
   break;   
   
   case 'c': 
   xbee.write("ATCN\r");
   break;  */
 
 }
}
 
 
void oscEvent(OscMessage theOscMessage) {
  /* check if theOscMessage has the address pattern we are looking for. */
  
  if(theOscMessage.checkAddrPattern("/light/forward")==true) {
    /* check if the typetag is the right one. */
      /* parse theOscMessage and extract the values from the osc message arguments. */
      //int speedLeftValue = theOscMessage.get(0).intValue();
      //int speedRightValue = theOscMessage.get(1).intValue();  
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      
      //SEND THE MESSAGE TO THE XBEE RADIO
           //Enter AT mode
        /*   xbee.write('+'); xbee.write('+'); xbee.write('+');
           delay(20);
           //Set destination address
           xbee.write("ATDL40BB71E0\r");
           delay(20);
           //Exit AT mode
           xbee.write("ATCN\r");
           delay(20);
           //Send the direction and speed command
           xbee.write('F');
           xbee.write(0x01);
           xbee.write(0x01);*/
           
           xbee.write(0x7E);
           xbee.write(0x00); //length
           xbee.write(0x12); //length
           xbee.write(0x10); //APi transmit request
           xbee.write(0x00); //no response
           xbee.write(0x00);
           xbee.write(0x13);
           xbee.write(0xA2);
           xbee.write(0x00);
           xbee.write(0x40);
           xbee.write(0xBB);
           xbee.write(0x71);
           xbee.write(0xE0);
           xbee.write(0xFF);
           xbee.write(0xFE);
           xbee.write(0x00); //radius
           xbee.write(0x00); //options
           xbee.write(0x46);
           xbee.write(0x31);
           xbee.write(0x30);
           xbee.write(0x30); //end of payload
           //long sum = 0x10 + 0x13 + 0xA2 + 0x40 + 0xBB + 0x71 + 0xE0 + 0xFF + 0xFE + 0x46 + 0x31 + 0x30 + 0x30;
          // long checksum = (0xFF - (sum & 0xFF));
           long checksum = 0xFF - (0x08 + 0x01 + 0x4E + 0x4A + 0xFF);
           println(checksum);
           xbee.write((byte)checksum);
                  
      print("### received an osc message");
      //println(" with speed value: "+speedLeftValue + " " + speedRightValue);
      return; 
  } 
  
  if(theOscMessage.checkAddrPattern("/light/backward")==true) {
    /* check if the typetag is the right one. */
      /* parse theOscMessage and extract the values from the osc message arguments. */
      int speedLeftValue = theOscMessage.get(0).intValue();  
      int speedRightValue = theOscMessage.get(1).intValue(); 
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      
      //SEND THE MESSAGE TO THE XBEE RADIO
           //Enter AT mode
           /*xbee.write('+'); xbee.write('+'); xbee.write('+');
           delay(20);
           //Set destination address
           xbee.write("ATDL40BB71E0\r");
           delay(10);
           //Exit AT mode
           xbee.write("ATCN\r");
           delay(20);*/
           //Send the direction and speed command
           xbee.write('B');
           xbee.write(speedLeftValue);
           xbee.write(speedRightValue);
      print("### received an osc message");
      println(" with speed value: "+speedLeftValue + " " + speedRightValue);
      return; 
  }
  
    if(theOscMessage.checkAddrPattern("/light/left")==true) {
    /* check if the typetag is the right one. */
      /* parse theOscMessage and extract the values from the osc message arguments. */
      int speedLeftValue = theOscMessage.get(0).intValue();  
      int speedRightValue = theOscMessage.get(1).intValue();
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      
      //SEND THE MESSAGE TO THE XBEE RADIO
           //Enter AT mode
           /*xbee.write('+'); xbee.write('+'); xbee.write('+');
           delay(20);
           //Set destination address
           xbee.write("ATDL40BB71E0\r");
           delay(10);
           //Exit AT mode
           xbee.write("ATCN\r");
           delay(20);*/
           //Send the direction and speed command
           xbee.write('L');
           xbee.write(speedLeftValue);
           xbee.write(speedRightValue);
      print("### received an osc message");
      println(" with speed value: "+speedLeftValue + " " + speedRightValue);
      return; 
  }
  
    if(theOscMessage.checkAddrPattern("/light/right")==true) {
    /* check if the typetag is the right one. */
      /* parse theOscMessage and extract the values from the osc message arguments. */
      int speedLeftValue = theOscMessage.get(0).intValue();  
      int speedRightValue = theOscMessage.get(1).intValue();
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      
      //SEND THE MESSAGE TO THE XBEE RADIO
           //Enter AT mode
           /*xbee.write('+'); xbee.write('+'); xbee.write('+');
           delay(20);
           //Set destination address
           xbee.write("ATDL40BB71E0\r");
           delay(10);
           //Exit AT mode
           xbee.write("ATCN\r");
           delay(20);*/
           //Send the direction and speed command
           xbee.write('R');
           xbee.write(speedLeftValue);
           xbee.write(speedRightValue);
      print("### received an osc message");
      println(" with speed value: "+speedLeftValue + " " + speedRightValue);
      return; 
  }
  
      if(theOscMessage.checkAddrPattern("/light/stop")==true) {
    /* check if the typetag is the right one. */
      /* parse theOscMessage and extract the values from the osc message arguments. */
      int speedLeftValue = theOscMessage.get(0).intValue();  
      int speedRightValue = theOscMessage.get(1).intValue();   
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      
      //SEND THE MESSAGE TO THE XBEE RADIO
           //Enter AT mode
           /*xbee.write('+'); xbee.write('+'); xbee.write('+');
           delay(20);
           //Set destination address
           xbee.write("ATDL40BB71E0\r");
           delay(10);
           //Exit AT mode
           xbee.write("ATCN\r");
           delay(20);*/
           //Send the direction and speed command
           xbee.write('S');
           xbee.write(speedLeftValue);
           xbee.write(speedRightValue);
      print("### received an osc message");
      println(" with speed value: "+speedLeftValue + " " + speedRightValue);
      return; 
  }
}