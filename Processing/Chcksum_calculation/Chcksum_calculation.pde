byte a = (byte) 0x01;
byte b = (byte) 0x01;
byte c = (byte) 0x50;
byte d = (byte) 0x01;
byte f = (byte) 0x00;

byte g = (byte) 0x48;
byte h = (byte) 0x65;
byte i = (byte) 0x6C;
byte l = (byte) 0x6C;
byte m = (byte) 0x6F;

byte check = (byte) (a+b+c+d+f+g+h+i+l+m);

byte diff =  (byte) (0xFF - check);

byte summ = (byte) (check + diff);

println(hex(check));
println(hex(diff));

println(hex(summ));